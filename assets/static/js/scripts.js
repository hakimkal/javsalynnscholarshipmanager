jQuery(document).ready(function($) {

    var owlTwo = $("#owlTwo");
        owlTwo.owlCarousel({
            items : 1, 
            loop : true,
            autoplay:false,
            autoplayTimeout: 5000,
            itemsDesktop : [992,1],
            itemsDesktopSmall : [768,1], 
            itemsTablet: [480,1], 
            itemsMobile : [320,1]
        });

    var owlOne = $("#owlOne");
        owlOne.owlCarousel({
            items : 1, 
            loop : true,
            autoplay:true,
            autoplayTimeout: 10000,
            itemsDesktop : [992,1],
            itemsDesktopSmall : [768,1], 
            itemsTablet: [480,1], 
            itemsMobile : [320,1]
        });


        $('#addSponsor').on('click', function() {
            let inputName = 1;
            $(`<div class="form-group col-12">
                <input type="text" name=${inputName++} class="form-control custom-apply-input">
            </div>`).appendTo('#sponsorFields');   
      });

      $('#addSponsorFields').on('click', function() {
        let sponsor = 1;
        $(`<div>
                <label class="secondary-apply-label">SPONSOR ${sponsor++}</label>
                <div class="form-group">
                    <label class="secondary-apply-label">CONTACT ADDRESS</label>
                    <input name="sponsorAddress${sponsor++}" type="text" class="form-control custom-apply-input">
                </div>
                <div class="form-group">
                    <label class="secondary-apply-label">EMAIL ADDRESS</label>
                    <input name="sponsorEmail${sponsor++}" type="text" class="form-control custom-apply-input">
                </div>
                <div class="form-group">
                    <label class="secondary-apply-label">MOBILE NO.</label>
                    <input name="sponsorPhone${sponsor++}" type="text" class="form-control custom-apply-input">
                </div>
            </div>`).appendTo('#sponsorInputs');   
  });

});