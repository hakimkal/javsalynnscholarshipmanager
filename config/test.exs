use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :javsalynn_smanager, JavsalynnSmanagerWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :javsalynn_smanager, JavsalynnSmanager.Repo,
  username: "postgres",
  password: "select",
  database: "javsalynn_smanager_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
