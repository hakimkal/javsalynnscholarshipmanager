defmodule JavsalynnSmanager.Repo do
  use Ecto.Repo,
    otp_app: :javsalynn_smanager,
    adapter: Ecto.Adapters.Postgres
end
