defmodule JavsalynnSmanager.Account.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias JavsalynnSmanager.Account.User

  schema "users" do
    field :email,                 :string
    field :encrypted_password,    :string
    field :firstname,             :string
    field :lastname,              :string
    field :phone,                 :string
    field :password,              :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:firstname, :lastname, :email, :password_confirmation, :password, :phone])
    |> validate_required([:firstname, :lastname, :phone, :email,   :password, :password_confirmation])
    |> validate_format(:email, ~r/@/, message: "is invalid")
    |> validate_length(:password, min: 6, max: 100)
    |> validate_confirmation(:password, message: "does not match password")
    |> unique_constraint(:email)
    |> put_encrypted_password
  end

  defp put_encrypted_password(changeset) do
    case changeset.valid? do
      true ->
        changes = changeset.changes
        put_change(changeset, :encrypted_password,  Bcrypt.hash_pwd_salt(changes.password))
      _ ->
        changeset
    end
  end
end
