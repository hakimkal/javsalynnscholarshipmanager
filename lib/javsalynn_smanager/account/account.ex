defmodule JavsalynnSmanager.Account do
  alias JavsalynnSmanager.Repo
  alias JavsalynnSmanager.Account.User

  def get_user!(id), do: Repo.get!(User, id)

  def list_users do
    Repo.all(Users)
  end

  def build_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
  end

  def change_user(%User{} = user, attrs \\ %{}) do
    user
    |> User.changeset( attrs)
  end

  def create_user(attrs) do
    attrs
    |> build_user
    |> Repo.insert
  end

  def get_user!(id) do
      Repo.get!(User, id)
      |> Repo.preload(:user)
  end

  def get_user_by_email(email), do: Repo.get_by(User, email: email)

  def get_user_by_credentials(%{"email" => email, "password" => pass} ) do
    user = get_user_by_email(email)
    cond do
      user && Bcrypt.verify_pass(pass, user.encrypted_password) ->
        user
      true ->
        :error
    end
  end
end
