defmodule JavsalynnSmanagerWeb.Router do
  use JavsalynnSmanagerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end


  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :frontend  do
    plug JavsalynnSmanagerWeb.Plugs.LoadUser
    
  end

  scope "/", JavsalynnSmanagerWeb do
    pipe_through :browser

    
    get "/login", SessionController, :new
    post "/login", SessionController, :create
    get "/register", RegistrationController, :new
    post "/register", RegistrationController, :create
   
    
  end
  scope "/", JavsalynnSmanagerWeb do
    pipe_through [:browser, :frontend, JavsalynnSmanagerWeb.Plugs.AuthUser]

    get "/", PageController, :index
    get "/logout", SessionController, :delete
    
  end
  # Other scopes may use custom stacks.
  # scope "/api", JavsalynnSmanagerWeb do
  #   pipe_through :api
  # end
end
