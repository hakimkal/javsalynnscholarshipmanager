defmodule JavsalynnSmanagerWeb.RegistrationController do
  use JavsalynnSmanagerWeb, :controller
  alias JavsalynnSmanager.Account
  alias JavsalynnSmanager.Account.User

  

  def new(conn, _) do
    changeset = Account.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end
  

  def create(conn, %{"registration" => registration_params}) do
    case Account.create_user(registration_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Registration successful")
        |> redirect(to: Routes.page_path(conn, :index))
      {:error, changeset} ->
      changeset|> IO.inspect
        conn
        |> render(:new, changeset: changeset)
    end
  end
end
