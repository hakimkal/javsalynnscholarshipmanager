defmodule JavsalynnSmanager.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    execute("CREATE EXTENSION IF NOT EXISTS citext")
    create table(:users) do
      add :firstname, :string
      add :lastname, :string
      add :email, :citext
      add :phone, :string
      add :encrypted_password, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
